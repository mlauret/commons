import Vue from 'vue'

export default function (ctx, inject) {
  const order = Vue.observable({
    data: {
      name: '',
      species: ''
    }
  })

  order.set = (data) => {
    order.data = { ...order.data, ...data }
  }

  ctx.$order = order
  inject('order', order)
}
