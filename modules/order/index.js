const { join } = require('path')

module.exports = function () {
  this.addPlugin({
    src: join(__dirname, './plugin.js'),
    fileName: 'order/plugin.js'
  })

  this.addPlugin({
    src: join(__dirname, './middleware.js'),
    fileName: 'order/middleware.js'
  })
}
