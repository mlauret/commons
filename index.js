const { join } = require('path')

module.exports = function () {
  this.nuxt.hook('components:dirs', (dirs) => {
    dirs.push({
      path: join(__dirname, 'components/order'),
      extensions: ['vue'],
      pathPrefix: false,
      prefix: 'Order'
    })
  })
  this.nuxt.hook('components:dirs', (dirs) => {
    dirs.push({
      path: join(__dirname, 'components/form'),
      extensions: ['vue'],
      pathPrefix: false,
      prefix: 'Form'
    })
  })

  this.addPlugin({
    src: join(__dirname, './modules/order/plugin.js'),
    fileName: 'order/middleware.js'
  })
}
